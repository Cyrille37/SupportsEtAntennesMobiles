<?php
/**
 * Return some CartoRadio's supports inside a bounding box.
 */

include __DIR__.'/config.php' ;

$db = new PDO( $config['dsn'], $config['db_user'], $config['db_password'] );
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//error_log( print_r($_REQUEST, true) );

// [bbox] => 0.8614826202392579,47.47602772129793,0.9734058380126954,47.49922773516302
$bbox = explode(',', $_REQUEST['bbox']);
$minx = $bbox[0];
$miny = $bbox[1];
$maxx = $bbox[2];
$maxy = $bbox[3];

$sql = 'SELECT * FROM opencellid.supports_anfr';
$sql.= ' WHERE lon>=? AND lon<=? AND lat>=? AND lat<=?' ;

$sth = $db->prepare( $sql );
$sth->execute( [$minx, $maxx, $miny, $maxy] );

# Build GeoJSON feature collection array
$geojson = array(
   'type'      => 'FeatureCollection',
   'features'  => array()
);

# Loop through rows to build feature arrays
while ($row = $sth->fetch(PDO::FETCH_ASSOC))
{
    $lon = $row['lon']; unset($row['lon']);
    $lat = $row['lat']; unset($row['lat']);
    $feature = array(
        'type' => 'Feature',
        'geometry' => array(
            'type' => 'Point',
            'coordinates' => array( $lon, $lat )
        ),
        'properties' => $row
    );
    # Add feature arrays to feature collection array
    array_push( $geojson['features'], $feature );
}

header('Content-type: application/json');
echo json_encode($geojson, JSON_NUMERIC_CHECK);
