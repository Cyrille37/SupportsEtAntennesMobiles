<?php

include __DIR__.'/config.php' ;

$db = new PDO( $config['dsn'], $config['db_user'], $config['db_password'] );
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

error_log('coucou');
error_log( print_r($_REQUEST, true) );

// [bbox] => 0.8614826202392579,47.47602772129793,0.9734058380126954,47.49922773516302
$bbox = explode(',', $_REQUEST['bbox']);
$minx = $bbox[0];
$miny = $bbox[1];
$maxx = $bbox[2];
$maxy = $bbox[3];

$sql = 'select radio, mcc, mnc, lac, cellid, unit, X(pt) as lon, Y(pt) as lat, `range`, samples, changeable, created_at, updated_at, average_signal from opencellid.cells';
$sql.= ' where MBRContains( LineString( Point(?,?), Point(?,?), Point(?,?), Point(?,?), Point(?,?) ), pt ) ' ;
$sth = $db->prepare( $sql );
$sth->execute( [$minx, $miny, $minx, $maxy, $maxx, $maxy, $maxx, $miny, $minx, $miny] );

# Build GeoJSON feature collection array
$geojson = array(
   'type'      => 'FeatureCollection',
   'features'  => array()
);

# Loop through rows to build feature arrays
while ($row = $sth->fetch(PDO::FETCH_ASSOC))
{
    $lon = $row['lon']; unset($row['lon']);
    $lat = $row['lat']; unset($row['lat']);
    $feature = array(
        'type' => 'Feature',
        'geometry' => array(
            'type' => 'Point',
            'coordinates' => array( $lon, $lat )
        ),
        'properties' => $row
    );
    # Add feature arrays to feature collection array
    array_push( $geojson['features'], $feature );
}

header('Content-type: application/json');
echo json_encode($geojson, JSON_NUMERIC_CHECK);
