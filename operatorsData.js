/**
 * sources liste IMSI (International Mobile Subscriber Identity)
 * https://fr.wikipedia.org/wiki/Mobile_Network_Code
 * https://www.itu.int/dms_pub/itu-t/opb/sp/T-SP-E.212B-2014-PDF-E.pdf
 * http://www.arcep.fr/fileadmin/reprise/interactive/numero/liste-imsi.pdf
 */

var Operators = {

	getOperatorByMccMnc: function( mcc, mnc )
	{
		var mcc_mnc = {
			208: {
				1: { label: 'FRTE', title: 'Orange France, GSM et UMTS (principal)', color: '#123456', icon: null },
				2: { label: 'FRTE', title: 'Orange France, itinérance zones blanches', color: '#123456', icon: null },
				3: { label: 'MOQU', title: 'MobiquiThings (full MVNO)', color: '#123456', icon: null },
				4: { label: 'SIST', title: 'Sisteer (MVNE, full MVNO)', color: '#123456', icon: null },
				5: { label: 'GLOB', title: 'Globalstar Europe (Satellite)', color: '#123456', icon: null },
				6: { label: 'GLOB', title: 'Globalstar Europe', color: '#123456', icon: null },
				7: { label: 'GLOB', title: 'Globalstar Europe', color: '#123456', icon: null },
				8: { label: 'COMP', title: 'Completel (Full MVNO)', color: '#123456', icon: null },
				9: { label: 'SFR0', title: 'Neuf Cégétel / SFR', color: '#123456', icon: null },
				10: { label: 'SFR0', title: 'SFR, GSM/UMTS (principal)', color: '#123456', icon: null },
				11: { label: 'SFR0', title: 'SFR, UMTS (Femtocell)', color: '#123456', icon: null },
				13: { label: 'SFR0', title: 'SFR, GSM/UMTS (zones blanches)', color: '#123456', icon: null },
				14: { label: 'RFF', title: 'RFF GSM-R (réseau privé)', color: '#123456', icon: null },
				15: { label: 'FRMO', title: 'Free Mobile UMTS (principal)', color: '#123456', icon: null },
				16: { label: 'FRMO', title: 'Free Mobile (Femtocells ?)', color: '#123456', icon: null },
				17: { label: 'LEGO', title: 'LEGOS - LOCAL EXCHANGE GLOBAL OPERATION SERVICES', color: '#123456', icon: null },
				20: { label: 'BOUY', title: 'Bouygues Télécom, GSM/UMTS (principal)', color: '#123456', icon: null },
				21: { label: 'BOUY', title: 'Bouygues Télécom (expérimental)', color: '#123456', icon: null },
				22: { label: 'TRAT', title: 'Transatel (MVNE)', color: '#123456', icon: null },
				23: { label: 'OTSE', title: 'Omer Telecom Ltd et Virgin Mobile (Full MVNO)', color: '#123456', icon: null },
				24: { label: 'MOQU', title: 'MobiquiThings', color: '#123456', icon: null },
				25: { label: 'LYCA', title: 'Lycamobile et GT-Mobile (Full MVNO)', color: '#123456', icon: null },
				26: { label: 'NRJ', title: 'NRJ Mobile (MVNO et Full MVNO)', color: '#123456', icon: null },
				27: { label: 'AFON', title: 'Afone (MVNE)', color: '#123456', icon: null },
				28: { label: 'ASSA', title: 'Astrium SAS (satellite)', color: '#123456', icon: null },
				29: { label: 'IMCO', title: 'International mobile Communication (Full MVNO)', color: '#123456', icon: null },
				30: { label: 'SYMA', title: 'Symacom (Full MVNO)', color: '#123456', icon: null },
				31: { label: 'MUND', title: 'Mundio Mobile (MVNE)', color: '#123456', icon: null },
				88: { label: 'BOUY', title: 'Bouygues Télécom, GSM/UMTS (zones blanches)', color: '#123456', icon: null },
				89: { label: 'OTSE', title: 'Omer Telecom (Test)', color: '#123456', icon: null },
				90: { label: 'IMRE', title: 'Association Images et réseaux (Test)', color: '#123456', icon: null },
				91: { label: 'FRTE', title: 'Orange France (test)', color: '#123456', icon: null },
				92: { label: 'PFTL', title: 'Association Plate-forme Telecom (Test)', color: '#123456', icon: null },
				93: { label: 'TDF', title: 'TDF', color: '#123456', icon: null }
			},
			308: {
				1: { label: 'SPMT', title: 'SAS St. Pierre-et-Miquelon Télécom', color: '#123456', icon: null },
				2: { label: 'GLOA', title: 'Globaltel', color: '#123456', icon: null }
			},
			340: {
				1: { label: 'ORCA', title: 'ORANGE CARAÏBE MOBILES', color: '#123456', icon: null },
				2: { label: 'OUTR', title: 'OUTREMER TELECOM', color: '#123456', icon: null },
				3: { label: 'STMB', title: 'CARAIBES', color: '#123456', icon: null },
				8: { label: 'DAUF', title: 'DAUPHIN TELECOM', color: '#123456', icon: null },
				10: { label: 'GTPM', title: 'GUADELOUPE TELEPHONE MOBILE', color: '#123456', icon: null },
				11: { label: 'GYPM', title: 'GUYANE TELEPHONE MOBILE', color: '#123456', icon: null },
				12: { label: 'MTPM', title: 'MARTINIQUE TELEPHONE MOBILE', color: '#123456', icon: null },
				20: { label: 'BOUY', title: 'BOUYGUES TELECOM', color: '#123456', icon: null }
			}
		};

		if( mcc_mnc[mcc] && mcc_mnc[mcc][mnc] )
		{
			return mcc_mnc[mcc][mnc] ;
		}
		console.log( 'unknow '+mcc+'-'+mnc);
		return {
			label: '?',
			title: 'unknow operator',
			color: '#000000'
		};
	}

}
