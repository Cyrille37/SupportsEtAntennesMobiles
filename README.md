# Supports et Antennes mobiles

Agrégation de données sur les antennes radio pour la téléphonie mobile.

* Dans une base MySql
 * cells ([OpenCellId.org](http://opencellid.org))
 * supports ([ANFR](http://anfr.fr))
* Affichage sur une carte
 * [index.html](index.html): leaflet and plugins to display data
 * [getCells.php](getCells.php): to query mesured cells position
 * [getSupportsANFR.php](getSupportsANFR.php): to query ANFR antennas supports
 * Les structures présentent dans OSM via [overpass-api](http://wiki.openstreetmap.org/wiki/Overpass_API)

## Source de données

* Les données du projet [OpenCellId.org](http://opencellid.org) sont mise à disposition par Mozilla
 * https://location.services.mozilla.com/downloads
 * script SQL pour l'import: [scripts/import_opencellid.sql](scripts/import_opencellid.sql)
* Les données de l'[ANFR](http://anfr.fr) sont mise à disposition en "Licence Ouverte / Open Licence" sur EtaLab
 * ces données sont visibles sur une carte sur [cartoradio.fr](http://www.cartoradio.fr)
 * https://www.data.gouv.fr/fr/datasets/donnees-sur-les-installations-radioelectriques-de-plus-de-5-watts-1/
 * script SQL pour l'import: [scripts/installations-radioelectriques-import.sql](scripts/installations-radioelectriques-import.sql)

## Divers

* Demande intégration des données "supports antennes" de l'ANFR dans [OSMOSE](http://osmose.openstreetmap.fr/): https://github.com/osm-fr/osmose-backend/issues/106

