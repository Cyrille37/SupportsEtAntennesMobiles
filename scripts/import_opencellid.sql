/**
 * Import opencellid csv database.
 */

CREATE DATABASE IF NOT EXISTS opencellid ;
USE opencellid ;

DROP TABLE IF EXISTS cells ;
CREATE TABLE cells
(
	radio CHAR(4) NOT NULL,
	mcc INTEGER,
	mnc INTEGER,
	lac INTEGER,
	cellid INTEGER,
	unit INTEGER,
	-- lon DOUBLE NOT NULL,
	-- lat DOUBLE NOT NULL,
	pt POINT NOT NULL,
	`range` INTEGER,
	samples INTEGER,
	changeable BOOLEAN,
	created_at TIMESTAMP,
	updated_at TIMESTAMP,
	average_signal INTEGER
)
ENGINE=MyISAM;

-- CREATE INDEX cells_idx ON cells(mcc, mnc, lac, cellid);
-- CREATE INDEX cells_radio ON cells(radio);
-- CREATE INDEX cells_idx ON cells(mcc, mnc, lac);
-- CREATE INDEX cells_lon ON cells(lon);
-- CREATE INDEX cells_lat ON cells(lat);
CREATE SPATIAL INDEX cells_pt on cells(pt);

-- Limit: with the API KEY one download by day, poor bandwitdth
--	LOAD DATA /*LOCAL*/ INFILE '/home/cyrille/Code/www/OpenCellId.WebMirror/cell_towers.csv'
-- Same data hosted by Mozilla. No download limitation and (very) big bandwidth:
LOAD DATA /*LOCAL*/ INFILE '/home/cyrille/Code/www/OpenCellId.WebMirror/MLS-full-cell-export-2016-03-28T000000.csv'
-- to make a extract, like FRANCE (208) ORANGE (1):
--	grep '^[A-Z]\+,208,1,' MLS-full-cell-export-2016-03-28T000000.csv > OpenCellId.WebMirror/MLS-full-cell-export-2016-03-28T000000.EXTRACT-FR-Orange.csv
-- LOAD DATA /*LOCAL*/ INFILE '/home/cyrille/Code/www/OpenCellId.WebMirror/MLS-full-cell-export-2016-03-28T000000.EXTRACT-FR-Orange.csv'
INTO TABLE cells
FIELDS terminated by ','
IGNORE 1 LINES 
(
	radio, mcc, mnc, lac, cellid, unit, @varlon, @varlat, `range`, samples, changeable, @varcreated_at, @varupdated_at, average_signal
)
SET pt = GeomFromText(concat('POINT(',@varlon,' ',@varlat,')')),
created_at = FROM_UNIXTIME(@varcreated_at),
updated_at = FROM_UNIXTIME(@varupdated_at)
;
