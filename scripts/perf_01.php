#!/usr/bin/env php
<?php
/**
 * Test performance
 * 
 * with indexes:
 *	INDEX cells_lonceil ON cells(lon_ceil);
 *	INDEX cells_latceil ON cells(lat_ceil);
 */

include __DIR__.'/../config.php' ;

$db = new PDO( $config['dsn'], $config['db_user'], $config['db_password'] );

define( 'LOOPS', 1000 );
define( 'MUL', 100000 );
define( 'LON_MAX', 11.689 /*180.0*/ );
define( 'LON_MIN', -5.317 /*-180.0*/ );
define( 'LAT_MAX', 51.700 /*90.0*/ );
define( 'LAT_MIN', 42.147 /*-90.0*/ );

$bindings = Array(LOOPS) ;
for( $i=0; $i<LOOPS; $i++ )
{
	$p = Array(8);

	$lat1 = rand(LAT_MIN*MUL, LAT_MAX*MUL)/MUL;
	$lat2 = rand(LAT_MIN*MUL, LAT_MAX*MUL)/MUL;
	$lon1 = rand(LON_MIN*MUL, LON_MAX*MUL)/MUL;
	$lon2 = rand(LON_MIN*MUL, LON_MAX*MUL)/MUL;

	$p[0] = ceil( min($lat1, $lat2) );
	$p[1] = ceil( max($lat1, $lat2) );
	$p[2] = ceil( min($lon1, $lon2) );
	$p[3] = ceil( max($lon1, $lon2) );

	$p[4] = min($lat1, $lat2) ;
	$p[5] = max($lat1, $lat2) ;
	$p[6] = min($lon1, $lon2) ;
	$p[7] = max($lon1, $lon2) ;

	$bindings[$i] = $p ;
}

echo LOOPS, ' loops', "\n";
//test_with_latlon($db, $bindings);
test_with_point($db, $bindings);
//test_with_latlon_ceil($db, $bindings);

function test_with_latlon( $db, $p )
{
	echo '=== ', __FUNCTION__, "===\n";

	$sql = 'select count(*) from opencellid.cells';
	$sql.= ' where (lat >= ? and lat <= ?)' ;
	$sql.= ' and (lon >= ? and lon <= ?)' ;
	$sth = $db->prepare( $sql );

	$cellsCount = 0 ;

	$p2 = Array();
	for( $i=0; $i<LOOPS; $i++ )
	{
		$p2[$i] =  array_slice($p[$i], 4, 4);
	}

	$time_start = microtime(true);
	for( $i=0; $i<LOOPS; $i++ )
	{
		$sth->execute( $p2[$i] );
		$row = $sth->fetch(PDO::FETCH_NUM);
		$cellsCount	+= $row[0];
	}
	$time_end = microtime(true);

	echo 'Ellapsed  : ',($time_end-$time_start) ," s\n";
	echo 'Average   : ',($time_end-$time_start)/LOOPS ," s\n";
	echo 'cellsCount: ',$cellsCount ,"\n";
}

function test_with_point( $db, $p )
{
	echo '=== ', __FUNCTION__, "===\n";

	/*
	doc faux: POLYGON((MINX MINY, MAXX MINY, MAXX MAXY, MINX MAXY, MINX MINY))
	correct: POLYGON((MINX MINY, MINX MAXY, MAXX MAXY, MAXX MINY, MINX MINY))

	MBRContains est plus rapide que Contains !!!

		SET @g1 = GeomFromText('POLYGON((6.03 45.01, 6.03 45.08, 6.07 45.08, 6.07 45.01, 6.03 45.01))');
		-- SET @g2 = GeomFromText('Point(6.1 45.1)');
		-- SELECT Contains(@g1,@g2), Within(@g2,@g1);
		SELECT count(*) FROM opencellid.cells
		where Contains(@g1,pt) ;
	 */
	$sql = 'select count(*) from opencellid.cells';
	$sql.= ' where MBRContains( LineString( Point(?,?), Point(?,?), Point(?,?), Point(?,?), Point(?,?) ), pt ) ' ;

	$sth = $db->prepare( $sql );

	$cellsCount = 0 ;

	$p2 = Array();
	for( $i=0; $i<LOOPS; $i++ )
	{
		$p3 = array_slice($p[$i], 4, 4);
		$miny = $p3[0];
		$maxy = $p3[1] ;
		$minx = $p3[2];
		$maxx = $p3[3] ;
		$p2[$i] = [
			$minx, $miny, $minx, $maxy, $maxx, $maxy, $maxx, $miny, $minx, $miny
		];
	}

	$time_start = microtime(true);
	for( $i=0; $i<LOOPS; $i++ )
	{
		$sth->execute( $p2[$i] );
		$row = $sth->fetch(PDO::FETCH_NUM);
		$cellsCount	+= $row[0];
	}
	$time_end = microtime(true);

	echo 'Ellapsed  : ',($time_end-$time_start) ," s\n";
	echo 'Average   : ',($time_end-$time_start)/LOOPS ," s\n";
	echo 'cellsCount: ',$cellsCount ,"\n";
}

function test_with_latlon_ceil( $db, $p )
{
	echo '=== ', __FUNCTION__, "===\n";

	$sql = 'select count(*) from opencellid.cells';
	$sql.= ' where ((lat_ceil >= ? and lat_ceil <= ?)' ;
	$sql.= ' and (lon_ceil >= ? and lon_ceil <= ?))' ;
	$sql.= ' and ((lat >= ? and lat <= ?)' ;
	$sql.= ' and (lon >= ? and lon <= ?))' ;
	$sth = $db->prepare( $sql );

	$cellsCount = 0 ;

	$time_start = microtime(true);
	for( $i=0; $i<LOOPS; $i++ )
	{
		$sth->execute( $p[$i] );
		$row = $sth->fetch(PDO::FETCH_NUM);
		$cellsCount	+= $row[0];
	}
	$time_end = microtime(true);

	echo 'Ellapsed: ',($time_end-$time_start) ," seconds\n";
	echo 'Average : ',($time_end-$time_start)/LOOPS ," seconds\n";
	echo 'cellsCount: ',$cellsCount ,"\n";
}
