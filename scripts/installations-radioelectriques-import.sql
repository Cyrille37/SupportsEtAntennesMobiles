/**
 * Import des fichiers de 
 *	https://www.data.gouv.fr/fr/datasets/donnees-sur-les-installations-radioelectriques-de-plus-de-5-watts-1/
 */

USE opencellid ;

--
-- Table support_types
--

DROP TABLE IF EXISTS support_types ;
CREATE TABLE support_types (
	id INTEGER NOT NULL,
	label VARCHAR(255)
) ENGINE=MyISAM ;
ALTER TABLE support_types ADD PRIMARY KEY (`id`);

LOAD DATA INFILE '/home/cyrille/DATA/Mapping/installations radioélectriques/20160227_Tables_de_reference/SUP_NATURE.txt'
IGNORE
INTO TABLE support_types
FIELDS terminated by ';'
IGNORE 1 LINES 
(
	id, label
)
;

--
-- Table support_proprios
--

DROP TABLE IF EXISTS support_proprios ;
CREATE TABLE support_proprios (
	id INTEGER NOT NULL,
	label VARCHAR(255)
) ENGINE=MyISAM ;
ALTER TABLE support_proprios ADD PRIMARY KEY (`id`);

LOAD DATA INFILE '/home/cyrille/DATA/Mapping/installations radioélectriques/20160227_Tables_de_reference/SUP_PROPRIETAIRE.txt'
IGNORE
INTO TABLE support_proprios
FIELDS terminated by ';'
IGNORE 1 LINES 
(
	id, label
)
;

--
-- Table station_exploits
--

DROP TABLE IF EXISTS station_exploits ;
CREATE TABLE station_exploits (
	id INTEGER NOT NULL,
	label VARCHAR(255)
) ENGINE=MyISAM ;
ALTER TABLE station_exploits ADD PRIMARY KEY (`id`);

LOAD DATA INFILE '/home/cyrille/DATA/Mapping/installations radioélectriques/20160227_Tables_de_reference/SUP_EXPLOITANT.txt'
IGNORE
INTO TABLE station_exploits
FIELDS terminated by ';'
IGNORE 1 LINES 
(
	id, label
)
;

--
-- Table antenne_types
--

DROP TABLE IF EXISTS antenne_types ;
CREATE TABLE antenne_types (
	id INTEGER NOT NULL,
	label VARCHAR(255)
) ENGINE=MyISAM ;
ALTER TABLE antenne_types ADD PRIMARY KEY (`id`);

LOAD DATA INFILE '/home/cyrille/DATA/Mapping/installations radioélectriques/20160227_Tables_de_reference/SUP_TYPE_ANTENNE.txt'
IGNORE
INTO TABLE antenne_types
FIELDS terminated by ';'
IGNORE 1 LINES 
(
	id, label
)
;

--
-- table supports_anfr
--

DROP TABLE IF EXISTS supports_anfr ;
CREATE TABLE supports_anfr (
	id INTEGER NOT NULL,	-- SUP_ID
	-- STA_NM_ANFR (id antenne)
	type_id INTEGER NOT NULL,	-- NAT_ID
	proprio_id INTEGER NOT NULL DEFAULT 0,	-- TPO_ID
	lat DOUBLE NOT NULL, -- COR_NB_DG_LAT, COR_NB_MN_LAT, COR_NB_SC_LAT, COR_CD_NS_LAT
	lon DOUBLE NOT NULL, -- COR_NB_DG_LON, COR_NB_MN_LON, COR_NB_SC_LON, COR_CD_EW_LON
	hauteur FLOAT NOT NULL DEFAULT -1.0,	-- SUP_NM_HAUT
	adr_lieu  VARCHAR(255),
	adr_1 VARCHAR(255),
	adr_2 VARCHAR(255),
	adr_3 VARCHAR(255),
	adr_cp VARCHAR(10),
	adr_insee VARCHAR(10),
    proprio VARCHAR(255),
    exploitants VARCHAR(255)
) ENGINE=MyISAM ;
ALTER TABLE supports_anfr ADD PRIMARY KEY (`id`);
CREATE INDEX supports_lat on supports_anfr(lat);
CREATE INDEX supports_lon on supports_anfr(lon);

LOAD DATA /*LOCAL*/ INFILE '/home/cyrille/DATA/Mapping/installations\ radioélectriques/20160227_DATA/SUP_SUPPORT.txt'
IGNORE -- IGNORE DUPLICATE ENTRY
INTO TABLE supports_anfr
FIELDS terminated by ';'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES 
(
	@SUP_ID,@STA_NM_ANFR,@NAT_ID,
	@COR_NB_DG_LAT,@COR_NB_MN_LAT,@COR_NB_SC_LAT,@COR_CD_NS_LAT,
	@COR_NB_DG_LON,@COR_NB_MN_LON,@COR_NB_SC_LON,@COR_CD_EW_LON,
	@SUP_NM_HAUT,@TPO_ID,@ADR_LB_LIEU,@ADR_LB_ADD1,@ADR_LB_ADD2,@ADR_LB_ADD3,@ADR_NM_CP,@COM_CD_INSEE
)
SET id=@SUP_ID,
type_id=@NAT_ID,
proprio_id = IF(@TPO_ID='',0,@TPO_ID),
lat=IF(@COR_CD_NS_LAT='S',
	-1*(@COR_NB_DG_LAT+@COR_NB_MN_LAT/60.0+@COR_NB_SC_LAT/60.0/60.0),
    @COR_NB_DG_LAT+@COR_NB_MN_LAT/60.0+@COR_NB_SC_LAT/60.0/60.0
),
lon=IF(@COR_CD_EW_LON='W',
	-1*(@COR_NB_DG_LON+@COR_NB_MN_LON/60.0+@COR_NB_SC_LON/60.0/60.0),
    @COR_NB_DG_LON+@COR_NB_MN_LON/60.0+@COR_NB_SC_LON/60.0/60.0
),
hauteur=REPLACE(@SUP_NM_HAUT,',','.'),
adr_lieu=@ADR_LB_LIEU,
adr_1=@ADR_LB_ADD1,
adr_2=@ADR_LB_ADD2,
adr_3=@ADR_LB_ADD3,
adr_cp=@ADR_NM_CP,
adr_insee=@COM_CD_INSEE
;

--
-- table stations_anfr
--

DROP TABLE IF EXISTS stations_anfr ;
CREATE TABLE stations_anfr (
	-- ex: 02A0030020
	id CHAR(10) NOT NULL,	-- STA_NM_ANFR
	support_id INTEGER NOT NULL DEFAULT 0,
	exploitant_id INTEGER NOT NULL DEFAULT 0,	-- ADM_ID
    cartoradio_id INTEGER NOT NULL DEFAULT 0,	-- DEM_NM_COMSIS
    date_impl DATE,	-- Dte_Implantation
    date_modif DATE,
    date_service DATE
) ENGINE=MyISAM ;
ALTER TABLE stations_anfr ADD PRIMARY KEY (`id`);
CREATE INDEX stations_support_id on stations_anfr(support_id);

LOAD DATA /*LOCAL*/ INFILE '/home/cyrille/DATA/Mapping/installations radioélectriques/20160227_DATA/SUP_STATION.txt'
IGNORE
INTO TABLE stations_anfr
FIELDS terminated by ';'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES 
(
	@STA_NM_ANFR, @ADM_ID, @DEM_NM_COMSIS, @Dte_Implantation, @Dte_modif, @Dte_En_Service
)
SET
id = @STA_NM_ANFR,
support_id = 0,
exploitant_id = REPLACE(@ADM_ID,',','.'),
cartoradio_id = REPLACE(@DEM_NM_COMSIS,',','.'),
-- format: 15/10/2003
date_impl = IF( @Dte_Implantation='',DATE('0000-00-00'),DATE(concat( SUBSTRING(@Dte_Implantation,7,4),'-',SUBSTRING(@Dte_Implantation,4,2),'-',SUBSTRING(@Dte_Implantation,1,2) ))),
date_modif = IF( @Dte_modif='',DATE('0000-00-00'),DATE(concat( SUBSTRING(@Dte_modif,7,4),'-',SUBSTRING(@Dte_modif,4,2),'-',SUBSTRING(@Dte_modif,1,2) ))),
date_service = IF( @Dte_En_Service='',DATE('0000-00-00'),DATE(concat( SUBSTRING(@Dte_En_Service,7,4),'-',SUBSTRING(@Dte_En_Service,4,2),'-',SUBSTRING(@Dte_En_Service,1,2) )))
;

--
-- Match stations_anfr.support_id avec supports_anfr
-- 

DROP TABLE IF EXISTS TMP ;
CREATE TABLE TMP (
	SUP_ID INTEGER,
    STA_NM_ANFR CHAR(10)
);
CREATE INDEX TMP_STA_NM_ANFR on TMP(STA_NM_ANFR);

LOAD DATA INFILE '/home/cyrille/DATA/Mapping/installations\ radioélectriques/20160227_DATA/SUP_SUPPORT.txt'
IGNORE
INTO TABLE TMP
FIELDS terminated by ';'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES 
(
	SUP_ID,STA_NM_ANFR,
    @NAT_ID,
	@COR_NB_DG_LAT,@COR_NB_MN_LAT,@COR_NB_SC_LAT,@COR_CD_NS_LAT,
	@COR_NB_DG_LON,@COR_NB_MN_LON,@COR_NB_SC_LON,@COR_CD_EW_LON,
	@SUP_NM_HAUT,@TPO_ID,@ADR_LB_LIEU,@ADR_LB_ADD1,@ADR_LB_ADD2,@ADR_LB_ADD3,@ADR_NM_CP,@COM_CD_INSEE
);
UPDATE stations_anfr, TMP
	SET stations_anfr.support_id = TMP.SUP_ID
	WHERE stations_anfr.id = TMP.STA_NM_ANFR
;
DROP TABLE TMP ;

update supports_anfr T1
SET
	T1.proprio = (
		select label from support_proprios where support_proprios.id = T1.proprio_id
	),
	T1.exploitants = (
		select group_concat(distinct(label)) from stations_anfr T2
			left join station_exploits on (station_exploits.id = T2.exploitant_id)
		where T2.support_id = T1.id
		group by T2.support_id
	)
;
