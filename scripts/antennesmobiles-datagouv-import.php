#!/usr/bin/env php
<?php
/**
 * Import fichers des antennes mobiles en France.
 * 
 * Fichier édité par ANFR (http://www.cartoradio.fr)
 *	et disponible sur data.gouv.fr
 *	https://www.data.gouv.fr/fr/datasets/statistiques-sur-les-antennes-mobiles-en-france/
 * 
 * Temps de l'import: environ 16 minutes sont mon core i7 avec ssd.
 * 
 * Create tables:

DROP TABLE IF EXISTS supports ;
CREATE TABLE supports (
	numero INTEGER NOT NULL,
	pt POINT NOT NULL,
	insee CHAR(5) NOT NULL,
	lieu_dit VARCHAR(255),
	adresse VARCHAR(255),
	code_postal VARCHAR(10),
	commune VARCHAR(255),
	nature VARCHAR(255),
	hauteur DOUBLE,
	proprietaire VARCHAR(255)
) ENGINE=MyISAM ;
ALTER TABLE supports ADD PRIMARY KEY (`numero`);
CREATE SPATIAL INDEX supports_pt on supports(pt);

DROP TABLE IF EXISTS antennes ;
CREATE TABLE antennes (
	support_numero INTEGER NOT NULL,
	numero_cartoradio INTEGER NOT NULL,
	`type` VARCHAR(50),
	numero_antenne INTEGER NOT NULL,
	dimension DOUBLE,
	directivite VARCHAR(15),
	azimut DOUBLE,
	hauteur_sol DOUBLE,
	systeme VARCHAR(30),
	freq_debut DOUBLE,
	freq_fin DOUBLE,
	freq_unite CHAR(5)
) ENGINE=MyISAM ;
CREATE INDEX antennes_support_numero on antennes(support_numero);

 * 
 * Volume des données dans MySql (au 29/03/2016):
 * 
                Rows  Data Length  Index Length
  antennes   1140984   102.3 MiB    11.1 MiB
  cells     21586894     1.6 GiB     1.3 GiB
  supports     65382     8.0 MiB     4.6 MiB

 * 
 */

error_reporting(E_ALL);

require_once('/home/cyrille/Code/libs/php.libs/PHPExcel-1.8.1/Classes/PHPExcel.php');

include __DIR__.'/../config.php' ;

$folder = '/home/cyrille/Code/www/OpenCellId.WebMirror/antennes_mobiles' ;

$db = new PDO( $config['dsn'], $config['db_user'], $config['db_password'] );
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$time_start = microtime(true);

$db->beginTransaction();

$db->exec('DELETE FROM supports');
$db->exec('DELETE FROM antennes');

$rowsCount = process_files( $folder );

$db->commit();

$time_end = microtime(true);

echo 'Ellapsed time: ', ($time_end-$time_start), "s\n";
echo 'Total rows read: ', $rowsCount, "\n";

function process_files( $folder )
{
	// $fd = fopen($filename, 'r');
	
	$rowsCount = 0 ;

	$d = dir($folder);
	while( false !== ($entry = $d->read()) )
	{
		if( $entry == '.' || $entry == '..' || ! is_dir($folder.'/'.$entry) )
			continue ;

		$filepath = $folder.'/'.$entry.'/'.'Supports_Cartoradio.xls';
		if( ($fd = fopen( $filepath, 'r')) == false )
			die('Failed to open "'.$filepath.'"');

		$rowsCount += process_sheet( $filepath );
	}
	$d->close();
	
	return $rowsCount ;
}

function process_sheet( $filepath )
{
	echo __FUNCTION__, ' ', $filepath, "\n";

	$sheetsByName = [ 'Supports' => 0 , 'Antennes|Emetteurs|Bandes' => 1 ];

	$objPHPExcel = PHPExcel_IOFactory::load($filepath);

	echo "\t", 'reading supports ...', "\n";
	$sheet = $objPHPExcel->getSheet( $sheetsByName['Supports'] );
	$rowsCount1 = process_data_supports( $sheet->toArray() );
	echo "\t", 'rows read: ', $rowsCount1, "\n";

	echo "\t", 'reading antennes ...', "\n";
	$sheet = $objPHPExcel->getSheet( $sheetsByName['Antennes|Emetteurs|Bandes'] );
	$rowsCount2 = process_data_antennes( $sheet->toArray() );
	echo "\t", 'rows read: ', $rowsCount2, "\n";

	// Hyper important ;-)
	// https://github.com/PHPOffice/PHPExcel/Documentation/markdown/Overview/05-Deleting-a-Workbook.md
	$objPHPExcel->disconnectWorksheets();
	unset($objPHPExcel);

	return $rowsCount1 + $rowsCount2 ;
}

function process_data_antennes( Array $data )
{
	global $db ;

	$headers = [
		"Numéro de\nsupport",
		"Numéro\nCartoradio",
		"Type\nd'antenne",
		"Numéro\nd'antenne",
		"Dimension\nde l'antenne",
		'Directivité',
		'Azimut',
		'Hauteur/sol',
		'Système',
		'Début',
		'Fin',
		'Unité'
	];
	$sql = 'INSERT INTO antennes (support_numero,numero_cartoradio,`type`,numero_antenne,dimension,directivite,azimut,hauteur_sol,systeme,freq_debut,freq_fin,freq_unite)';
	$sql.= ' VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
	$sth = $db->prepare( $sql );

	$rowsCount = 0 ;
	foreach( $data as $row )
	{
		$rowsCount ++ ;
		if( $rowsCount==1 )
		{
			foreach( $row as $col )
			{
				if( ! in_array( $col, $headers ) )
					die('header "'.$col.'" not found');
			}
			continue ;
		}

		if( ! $sth->execute( $row ) )
		{
			die( print_r($db->errorInfo(),true)."\n");
		}
	}
	return $rowsCount ;
}

function process_data_supports( Array $data )
{
	global $db ;

	$headers = [
		"Numéro de \nsupport",
		'Longitude',
		'Latitude',
		'Position',
		'Insee',
		'Lieu dit',
		'Adresse',
		'Code postal',
		'Commune',
		'Nature du support',
		"Hauteur\nen m",
		'Propriétaire'
	];
	$sql = 'INSERT INTO supports (numero, pt, insee, lieu_dit, adresse, code_postal, commune, nature, hauteur, proprietaire)';
	$sql.= ' VALUES( ?, Point(?, ?), ?, ?, ?, ?, ?, ?, ?, ?)';
	$sth = $db->prepare( $sql );

	$rowsCount = 0 ;
	foreach( $data as $row )
	{
		$rowsCount ++ ;
		if( $rowsCount==1 )
		{
			foreach( $row as $col )
			{
				if( ! in_array( $col, $headers ) )
					die('header "'.$col.'" not found');
			}
			continue ;
		}

		// Not the right solution, binding need continious array indexes
		//unset( $row[ array_search('Position', $headers) ] );
		array_splice( $row, array_search('Position', $headers), 1);
		if( ! $sth->execute( $row ) )
		{
			die( print_r($db->errorInfo(),true)."\n");
		}
	}
	return $rowsCount ;
}
